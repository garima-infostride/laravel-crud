<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\AboutController;
use App\Http\Controllers\Frontend\CoursesController;
use App\Http\Controllers\Frontend\EventsController;
use App\Http\Controllers\Frontend\TrainersController;
use App\Http\Controllers\Frontend\PricingController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\CourseDetailsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index']);
Route::get('/about',[AboutController::class,'index']);
Route::get('/courses',[CoursesController::class,'index']);
Route::get('/trainers',[TrainersController::class,'index']);
Route::get('/events',[EventsController::class,'index']);
Route::get('/pricing',[PricingController::class,'index']);
Route::get('/contact',[ContactController::class,'index']);
Route::get('/courses/course-details',[CourseDetailsController::class,'index']);
